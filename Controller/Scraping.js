const rp = require('request-promise');
const cheerio = require('cheerio')
const Begin = async (req,res) =>{
    await rp('https://www.limetorrents.pro/')
    .then(html=>{
        const $ = cheerio.load(html)
        res.render( 'index',  {
            category: $('.lt-categories', html),
            titlePage: 'FreeTorrents'
        } )
        
        
    })
    .catch(err=>{
        console.log(err)
    })
}

const Detail = async (req,res) =>{
    const param = req.originalUrl ? req.originalUrl : ''
   await rp('https://www.limetorrents.pro'+ param)
    .then(html=>{
        const $ = cheerio.load(html)
      if(param.includes('.html')){
        res.render( 'download',  {
            title: $('#content h1', html),
            download: $('.torrentinfo', html).html(),
            rightbar: $('#rightbar div:eq(0)', html),
            output: $('#output', html).html(),
            fileline: {
                title: $('h2:eq(2)', html),
                content: $('.fileline', html)
            },
            mostSearch: $('center:eq(2)', html),
            titlePage: $('#content h2:eq(0)').html()
        } )
        console.log($('#content h2:eq(0)').html())
      }else{
        if(param.includes('/rss/') || param.includes('searchrss')){
            res.render( 'rss',  {
                fluxRss: html,
            } )
        }else{
            res.render( 'details',  {
                searchBox: $('#maincontentrouter #content', html),
                rightbar: $('#rightbar div:eq(0)', html),
                mostSearch: $('center:eq(2)', html),
                titlePage: $('#content h2:eq(0)').html()
            } )
        }
      }

       
        
        
    })
    .catch(err=>{
        console.log(err)
    })
}

const find =  (req,res) =>{
    rp('https://www.limetorrents.pro/search/all/'+ req.body.q)
    .then(html=>{
        
      
        const $ = cheerio.load(html)
        res.render( 'details',  {
            searchBox: $('#maincontentrouter #content', html),
            rightbar: $('#rightbar div:eq(0)', html),
            mostSearch: $('center:eq(2)', html),
            titlePage: 'Search'
        } )
        
        
    })
    .catch(err=>{
        console.log(err)
    })
}








module.exports =  {
    Begin,
    Detail,
    find
    
}
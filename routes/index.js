var express = require('express');
var router = express.Router();
var ControllerScraping = require('../Controller/Scraping')
/* GET scraping data . */
router.get('/', ControllerScraping.Begin);
router.get('/*', ControllerScraping.Detail);
router.post('/search', ControllerScraping.find);

module.exports = router;

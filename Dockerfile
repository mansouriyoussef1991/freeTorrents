FROM node:14
WORKDIR /var/www/node
COPY package*.json ./
RUN npm ci
COPY . .
EXPOSE 4000
function buildComment(result) {
    var opt = {
        items_per_page: 10,
        next_text: "Next >",
        num_display_entries: 10,
        num_edge_entries: 2,
        prev_text: "< Prev",
        callback: pageselectCallback
    };
    $("#Pagination").pagination(members.length, opt);
}

function pageselectCallback(page_index, jq) {
    var items_per_page = 10;
    var max_elem = Math.min((page_index + 1) * items_per_page, members.length);
    var newcontent = '';
    var len = members.length;
    var defAvatar = '/assets/images/pic-1.jpg';
    $('.comment-tab >a >span').html(len);
    for (var i = page_index * items_per_page; i < max_elem; i++) {
        var value = members[i];
        var avtr = value.avatar == '' ? defAvatar : value.avatar;
        //newcontent += '<div class="comment-detail"><div class="comment-info clearfix"><a class="frame ' + value.class + '" href="#"><img data-original="' + avtr + '" alt="" src="images/profile-load.svg" class="lazy"></a>' + '<div class="detail"><span class="arrow"></span><div class="user-name">' + '<small class="' + value.class + '"></small><a href="#">' + value.username + '</a><span><i class="flaticon-time"></i>' + value.posted + '</span></div>' + '<p>' + value.comment + '</p></div></div></div>';
        value.comment = value.comment.replace(/(?:\r\n|\r|\n)/g, '<br />');
        newcontent += '<div class="comment-detail"><div class="comment-info clearfix"><a class="frame ' + value.class + '" href="/user/' + value.username + '/"><img data-original="' + avtr + '" alt="" src="/images/profile-load.svg" class="lazy"></a>' + '<div class="detail"><span class="arrow"></span><div class="user-name">' + '<small class="' + value.class + '"></small><a href="/user/' + value.username + '/">' + value.username + '</a><span><i class="flaticon-time"></i>' + value.posted + '</span></div>' + '<p>' + value.comment + '</p></div></div></div>';
    }
    $('#comment').html(newcontent).promise().done(function() {
        $(window).scrollTop($('.comment-box').offset().top);
        $("img.lazy").lazyload({
          effect : "fadeIn",
          placeholder : 'images/profile-load.svg'
        });
    });
    return false;
}